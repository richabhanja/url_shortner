package com.ShortURLL.ShortURLL;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UrlShortenRestController {
	
	public String getRandomCharacters() {
		String randomStr ="";
		String possibleChar = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
		for(int i=0; i<5; i++)
			randomStr += possibleChar.charAt((int) Math.floor(Math.random()*possibleChar.length()));
		
		return randomStr;
	}
	
	private Map<String, ShortenURL> shortenUrlList = new HashMap<>();
	
	@RequestMapping(value="/shortenurl", method=RequestMethod.POST)
	public ResponseEntity<Object> getShortenUrl(@RequestBody ShortenURL url) throws  MalformedURLException{
		String randomChar = getRandomCharacters();
		setShortUrl(randomChar, url);
		return new ResponseEntity<Object>(url, HttpStatus.OK);
	}

	private void setShortUrl(String randomChar, ShortenURL url) throws  MalformedURLException{
		url.setShort_url("http://localhost:9090/s/"+randomChar);
		shortenUrlList.put(randomChar, url);
	}
	
	@RequestMapping(value="/s/{randomstring}", method=RequestMethod.GET)
	public void getFullUrl(HttpServletResponse response,@PathVariable("randomstring") String randomstring) throws  IOException{
		response.sendRedirect(shortenUrlList.get(randomstring).getFull_url());
	}
}
