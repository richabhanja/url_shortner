package com.ShortURLL.ShortURLL;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShortUrllApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShortUrllApplication.class, args);
	}

}
